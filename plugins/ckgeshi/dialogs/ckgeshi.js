

CKEDITOR.dialog.add('cmd_ckgeshi',function(editor) {
    return {
        title: editor.lang.ckgeshi.button,
        minWidth: 500, minHeight: 300,
        onShow:function() {
            var editor=this.getParentEditor();
			editor.ckgeshi.options.selectionInput(editor);
            this.setupContent(editor.ckgeshi.options);
        },
        onOk:function() {
            var editor=this.getParentEditor();
			this.commitContent(editor.ckgeshi.options);
			editor.ckgeshi.options.selectionOutput(editor);
        },
        contents: [{
			id: 'Config',
			label: editor.lang.ckgeshi.mainTab,
			elements: [{
				type: 'vbox',
				children: [{
					id: 'opt_language',
					type: 'select',
					labelLayout: 'horizontal',
					label: editor.lang.ckgeshi.language,
					style:	'font-weight: bold;',
					'default': 'JavaScript',
					widths: ['30%','60%'],
					items: editor.ckgeshi.available_languages,
					setup:function(data) {
						if (data.language) 
							this.setValue(data.language);
					},
					commit:function(data) {data.language=this.getValue();}
				}]
			}, {
				type : 'vbox',
				children: [{
					type: 'html',
					html: '<strong>' + editor.lang.ckgeshi.szMiscTitle + '</strong>'
				}, {
					id: 'opt_linenumbers',
					type: 'select',
					labelLayout: 'horizontal',
					label: editor.lang.ckgeshi.linenumbers,
					'default': 'default',
					widths: ['30%','60%'],
					items: [
						['default','default'],
						['off','off'],
						['normal','normal'],
						['fancy','fancy']
					],
					setup:function(data) {
						if (data.linenumbers!=null) {
							this.setValue(data.linenumbers);
						} else this.setValue('default');
					},
					commit:function(data) {
						if (this.getValue()!=null && this.getValue()!=='default')
							data.linenumbers=this.getValue();
						else data.linenumbers='default';
					}
				}, 
				{
					id: 'opt_fancy',
					type: 'select',
					labelLayout: 'horizontal',
					label: editor.lang.ckgeshi.fancy,
					'default': 'default',
					widths: ['75%','25%'],
					items: [
						['1','1'],
						['2','2'],
						['3','3'],
						['4','4'],
						['5','5'],
						['6','6'],
						['8','8'],
						['10','10'],
						['12','12'],
						['15','15'],
						['20','20'],
						['30','30'],
						['40','40'],
						['50','50']
					],
					setup:function(data) {
						if (data.fancy!=null && parseInt(data.fancy.replace('"',''))>0)
							this.setValue(data.fancy);
					},
					commit:function(data) {
						if (this.getValue() && parseInt(this.getValue())>0)
							data.fancy=this.getValue();
						else data.fancy=null;
					}
				}, {
					type: 	'html',
					html: 	editor.lang.ckgeshi.linenumbers_note+"<br /><br />",
					style:	'font-style: italic;'
				}, {
					type:	'vbox',
					labelLayout: 'horizontal',
					widths:	['90%','10%'],
					children: [{
						type: 'text',
						id:	  'opt_start',
						style: 'width: 10%;',
						maxlength: '3',
						label:	editor.lang.ckgeshi.start,
						setup: function(data) {
							if (data.start!=null && parseInt(data.start.replace('"',''))>0) 
								this.setValue(data.start);
						},
						commit: function(data) {
							var value=this.getValue();
							if (typeof(value)!='number') {
								value=parseInt(value,10);
								if (!value || typeof(value)!='number') value=null;
							}
							data.start=(value)?(''+value):null;
						}
					}]
				}, {
					type: 'html',
					html: '<br /><strong>' + editor.lang.ckgeshi.title + '</strong>'
				}, {
					type:	'vbox',
					labelLayout: 'horizontal',
					widths:	['30%','60%'],
					children: [{
						type: 'text',
						id:	  'opt_title',
						style: 'width: 40%;',
						label:	editor.lang.ckgeshi.title_note,
						setup: function(data) {
							if (data.title!=null && data.title.replace(' ','').length>0) 
								this.setValue(data.title);
						},
						commit: function(data) {
							if (this.getValue()!=null && this.getValue().replace(' ','').length>0) 
								data.title=this.getValue();
							else data.title=null;
						}
					}]
				}] // END: child entities
			}] // END: mainTab.elements
		}
		] // END: contents array
    };
});
