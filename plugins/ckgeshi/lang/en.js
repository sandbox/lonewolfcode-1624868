﻿CKEDITOR.plugins.setLang('ckgeshi', 'en',{
	ckgeshi: {
		button: 'CKGeshi Syntax Highlighter',
		mainTab: 'Syntax Block Config',
		
		language:	 'Select Language',
		szMiscTitle: 'Miscellaneous Options',
		linenumbers: 'Line-Numbering Mode: ',
		linenumbers_note: '\'Default\' Mode will defer to the Geshi-Module\'s internal settings.',
		fancy:		 'If using \'Fancy\' Mode, specify highlighting frequency:',
		start:		 'Start Line Numbering at (leave blank to start at default of 1): ',
		title:		 'Code-Region header Label:',
		title_note:	 'Note: Leave entry blank(empty) to remove it.'
	}
});
