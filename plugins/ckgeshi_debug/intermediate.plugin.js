/**
 * @file CKGeshi Plugin for CKEditor, integration of GeSHi Filter module.
 * 
 * Written by lonewolfcode
 * http://drupal.org/sandbox/lonewolfcode/1624868
 */

(function($) {
  var deepCopy_Volatiles = function() {
    return jQuery.extend(true, {}, Drupal.settings['ckeditor_geshi']); // clone the settings fed by drupal so that they remain persistant across multiple ckeditor entities.
  };
  // See Issue Request: http://drupal.org/node/1624880
  // Hook the ckeditorOff function, negating the need for patching the 'ckeditor.utils.js' file.
  Drupal.orig_ckeditorOff = Drupal.ckeditorOff;
  Drupal.ckeditorOff = function(textarea_id) {
    // these two logic-checks are performed by the original function, prior to performing anything
    // so they are duplicated here to preserve functionality:
    if (!CKEDITOR.instances || (typeof CKEDITOR.instances[textarea_id] === 'undefined')) return;
    if (!CKEDITOR.env.isCompatible) return;
	
	console.log('[Hook_ckeditorOff] performing notification event of volatile editor data.');
    // fire the event that notifies ckeditor is about to be destroyed (purpose of previous .patch file)
    CKEDITOR.instances[textarea_id].fire('beforeCKEditorOff');
    // and finally, call the original function:
    Drupal.orig_ckeditorOff(textarea_id);
  };
  
  var ckgeshi_cmd = 'cmd_ckgeshi';
  var GF_TYPE_ANGLE =   1;
  var GF_TYPE_SQUARE =  2;
  var GF_TYPE_DSQUARE = 4;
  // Start with DoubleSquare style, as single 'square' will pick up inside 'double' during regex scan , which would be fatal.
  var GFT_SCAN_ORDER = [GF_TYPE_DSQUARE, GF_TYPE_SQUARE, GF_TYPE_ANGLE];

  
CKEDITOR.plugins.add('ckgeshi', {
  requires: ['dialog'],
  lang: ['en'],
  
  beforeInit: function(editor) {
    // register the ckgeshi interface containing options and runtime code for events, and register the ckgeshi command for the toolbar
    iCKGeshi.prototype = new CKEDITOR.dialogCommand(ckgeshi_cmd);
    var _volatiles = deepCopy_Volatiles();
    console.log('[beforeInit('+editor.name+')]: \'pure\' volatiles retrieved:');
    console.log(_volatiles);
    
    // handler for multiple input formats, instead of using global tagStyles. (Configured by GeshiFilter module & thereafter within relative text input format)
    if (typeof _volatiles['formats'] !== 'undefined') { // custom array respective to input formats supporting ckeditor, provided by ckedit_geshi.module
      console.log('[beforeInit]: detected input-format-specific configuration: ');
      var current_fmt = Drupal.settings.ckeditor.elements[editor.name];
      for (var fmt in _volatiles['formats']) if (fmt === current_fmt) { // find the _current format_ of 'this' ckeditor instance
        _volatiles['codedef'] = _volatiles['formats'][fmt]; // store reference to it's code definitions for later usage.
        break;
      }
      console.log(_volatiles['codedef']);
    }
    // store 'ckgeshi' interface in 'editor' instance, for scope-related interaction :: most specifically in the dialog (which only has access to the 'editor')
    editor.ckgeshi = new iCKGeshi(editor, 'cmd_ckgeshi', this.path,_volatiles);
    editor.ckgeshi.listeners={};
	var _listeners=editor.ckgeshi.listeners;
	
    console.log('[beforeInit] using codedef: ');
    console.log(editor.ckgeshi.codearea.codedef);
    
    // register preProcess immediately, onGetData ( retrieved by ckeditor module, and used for ajax filtration)
    // we also need to hook into drupals CkEditor Toggle, so that we can detect when to postProcess and therefore also post::preProcess data.
    // Why not use the .on("destroy") event for postProcessing? Because the data is already dispatched to textarea by drupal for databse storage.
    // Thus, a custom event 'beforeCKEditorOff' MUST be fired PRIOR to true data output. See ckeditorOff hook above.
    //     Deprecated patch: 'ckeditor/includes/ckeditor.utils.js' :: @#168  &&  @#235
    editor.on("beforeCKEditorOff", _listeners.beforeDrupalFormSaved = function(e) {
	 editor.ckgeshi.postProcessNeeded = true; // only set flag, such that getData event may postProcess at the right & proper moment given an editor's context (allow it to perform logic checks normally).
      // toDo: verify that we need not force-fire the getdata function (to execute postProcess()) --- ie. does .updateElement() called by ajax form submit, execute getData() or rather only setData? 
    });
    editor.on('getData', _listeners.getData = CKEDITOR.tools.bind(function(e) {
	  if (this.preProcessNeeded) { // since getData is called frequently; we only want preProcess executed when necessary (on editor loadup, and thus, inherently, after shutdown and re-init . Notably for when user clicks the "Switch to Rich Text Editor" href on screen).
        this.preProcessNeeded = false;
		this.codearea.preProcess(e.data); // "byref"
        
		// obtain original submit handler if present and store it for later callback. // ToDo: Verify that ckeditor's bind will indeed overwrite normal dom submit handler -- thus making this step necessary. And if so, inspect what will occur when it also contains another ckeditor binding (possible multiple execution issue)
		this.origFormSubmit=this.editor.container.getAscendant("form", true).$.submit;
        // hook first available form parent, to postProcess the .type into .safeType; preserves the display of code to be highlighted
        this.editor.container.getAscendant("form", true).on("submit", this.listeners.submit = CKEDITOR.tools.bind(function(e) {
          // we should postProcess ALL instances, since form submit will finalize ANY ckeditor instance.
          // todo: verify operability during ajax usage, and in Views integrations.
          console.log(CKEDITOR.instances);
          for(var id in CKEDITOR.instances) {
		    // skip ckeditor instances that do not use ckgeshi
            if (!CKEDITOR.instances[id].ckgeshi) continue;
			// enable postProcessing on this ckeditor[id] instance
            CKEDITOR.instances[id].ckgeshi.postProcessNeeded = true;
          }
        },this));
      } else if (this.postProcessNeeded) {
        this.codearea.postProcess(e.data); // prepares data for the [geshifilter module] to use.
		this.postProcessNeeded = false;
		//this.preProcessNeeded = true; // When postProcessing, editor is generally destroyed hereafter, so this is reset anyway. But not when switching between rich-text-edit and plain-text-edit mode, or when ajax is used to submit the form.
      }
    },editor.ckgeshi));
  },
    init: function(editor, pluginPath) {
      // add the button and dialog for usage with the [ckgeshi command] in toolbar.
      editor.ui.addButton('CKGeshi', {
        label:   editor.lang.ckgeshi.button,
        command: ckgeshi_cmd,
        icon:    this.path+'images/ckgeshi.png'
      });
      CKEDITOR.dialog.add(ckgeshi_cmd, this.path+'dialogs/ckgeshi.js');
	  var _listeners=editor.ckgeshi.listeners;
	  
	  editor.addCommand('cregion_insertBefore',{exec: function(editor) {
			var self=editor.getSelection().getStartElement();
			if (CKEDITOR.config.enterMode === CKEDITOR.ENTER_P) {
			  var el=(new CKEDITOR.dom.element(editor.ckgeshi.enterTypes[CKEDITOR.config.enterMode]));
			  el.insertBefore(self);
			  el.append('br');
			}
			else (new CKEDITOR.dom.element(editor.ckgeshi.enterTypes[CKEDITOR.config.enterMode])).insertBefore(self);
	  }});
	  editor.addCommand('cregion_insertAfter',{exec: function(editor) {
			var self=editor.getSelection().getStartElement();
			if (CKEDITOR.config.enterMode === CKEDITOR.ENTER_P) {
			  var el=(new CKEDITOR.dom.element(editor.ckgeshi.enterTypes[CKEDITOR.config.enterMode]));
			  el.insertAfter(self);
			  el.append('br'); // is converted into <p>&nbsp;</p> onOutput of element to source mode and/or form submit.
			}
			else (new CKEDITOR.dom.element(editor.ckgeshi.enterTypes[CKEDITOR.config.enterMode])).insertAfter(self);
	  }});
      editor.addMenuGroup('ckgeshi_context');
	  editor.addMenuItem('cregion_insertBefore_id', {
		label: 'Insert line, Before code-region',
		command: 'cregion_insertBefore',
		group: 'ckgeshi_context'
	  });
	  editor.addMenuItem('cregion_insertAfter_id', {
		label: 'Insert line, After code-region',
		command: 'cregion_insertAfter',
		group: 'ckgeshi_context'
	  });
	  editor.contextMenu.addListener(CKEDITOR.tools.bind(function(element, selection) {
	    if (this.selectionIsCode) return { 
		    cregion_insertBefore_id: CKEDITOR.TRISTATE_OFF,
			cregion_insertAfter_id: CKEDITOR.TRISTATE_OFF
		};
		else return null;
	  },editor.ckgeshi));
	  
      // Register Event:: make toolbar button display properly when editor selection is a ckgeshi region
      editor.on('selectionChange', _listeners.selectionChange = CKEDITOR.tools.bind(function(e) {
        if (this.editor.readOnly) return;
        this.editor.getCommand(this.name).refresh(this.editor.getSelection(), this);
      }, editor.ckgeshi));
    
      // Register Events:: add element-rule handlers for the code-region dynamically (see ckgeshi.codearea and it's .type member).
      var dyn_rules_input={elements: {} }; dyn_rules_input.elements[editor.ckgeshi.codearea.type] =
        CKEDITOR.tools.bind(editor.ckgeshi.codearea.OnOutput,editor.ckgeshi);
      var dyn_rules_output={elements: {} }; dyn_rules_output.elements[editor.ckgeshi.codearea.type] =
        CKEDITOR.tools.bind(editor.ckgeshi.codearea.OnInput,editor.ckgeshi);
      editor.dataProcessor.htmlFilter.addRules(dyn_rules_input);
      editor.dataProcessor.dataFilter.addRules(dyn_rules_output);
    
      editor.on('focus', _listeners.focus = CKEDITOR.tools.bind(function(e) {
        // obtain all buttons that should be disabled when editing code.. (one-exeuction per editor instance)
        if (typeof this.blocked_commands === 'undefined') {
          this.blocked_commands = {}
          for (var cmd in this.editor._.commands) {
            if (typeof this.enabled_buttons_def[cmd] !=='undefined') continue;
			if (cmd === 'cregion_insertBefore') continue;
			if (cmd === 'cregion_insertAfter') continue;
            this.blocked_commands[cmd] = this.editor._.commands[cmd];
          }
        }
      }, editor.ckgeshi));
    
      // Webkit has a serious issue when performing text alterations on content; thus we must store & restore the 
      // scroll positions in webkit browsers. It should not affect other browsers negatively, therefore we do not perform browser check -- this is ALWAYS performed. (see events below)
      // ToDo: Verify only Y-direction is necessary.
      editor.on('saveScroll', _listeners.saveScroll = CKEDITOR.tools.bind(function(e) {
        this.editorScrollY = this.editor.document.$.body.scrollTop;
        this.screenScrollY = document.body.scrollTop;
      }, editor.ckgeshi));
      editor.on('restoreScroll', _listeners.restoreScroll = CKEDITOR.tools.bind(function(e) {
        this.editor.document.$.body.scrollTop = this.editorScrollY;
        document.body.scrollTop = this.screenScrollY;
      }, editor.ckgeshi));
    
      // Provide functionality for using enter, tab, & Paste keys inside code-region. 
      // toDo: verify proper interaction when Enter/Shift+Enter/Tab is pressed while selection contains code to be overwritten(currently the event is simply discarded). Firefox returns multiple range indexes in some cases, and may break logic conditions herein..
      editor.on('key', _listeners.key = function(e) {
        if (!e.editor.mode === 'wysiwyg') return; // because apparently the selectionIsCode can be 'true' when in 'source' mode. // toDo: locate reason for this, and stop it.
        if (!e.editor.ckgeshi.selectionIsCode) return;
        if (e.data.keyCode!==13 && e.data.keyCode!==(CKEDITOR.SHIFT+13) && e.data.keyCode!==9 && e.data.keyCode!==(CKEDITOR.SHIFT+9)) return;
        e.editor.fire('saveScroll');
        e.editor.fire('saveSnapshot'); // ToDo: Verify operability -- it "sometimes" does not permit an undo snapshot (when using enter key).
        CKEDITOR.tools.bind(e.editor.ckgeshi.onKey,e)(e.data.keyCode);
        e.editor.fire('restoreScroll');
      }, null, null, 5); // push the event to the top of ckeditor's listener stack
    
      // add support for paste operations...
      // toDo: read clipboard and filter the copied data. Data copied from within code-region also contains the codearea.type and it's attributes -- which is not generally desired.
	  //       this will require browser-specific code, unfortunately.
      editor.on('beforePaste', _listeners.beforePaste = function(e) {e.editor.fire('saveScroll');});
      editor.on('paste', _listeners.paste = function(e) {
        if (!e.editor.mode=='wysiwyg') return;
        if (!e.editor.ckgeshi.selectionIsCode) return;
        e.cancel();e.stop();
        e.editor.fire('saveSnapshot');
        var selection = e.editor.getSelection();
        var range=selection.getRanges()[0]; // todo: <see above comment>
        var data = ( (e.data.text)? e.data.text : e.data.html );
        e.editor.insertText(data);
        e.editor.fire('restoreScroll');
      }, null, null, 5); // push the event to the top of ckeditor's listener stack
	  
	  
	  editor.on('destroy', _listeners.destroy = CKEDITOR.tools.bind(function(e) {
		for (var lid in e.editor.ckgeshi.listeners) {
		  // this is very important, because submit handler bound on true DOM element such as the form, will concatenate with future bindings if not removed properly, as it persists outside the scope of ckeditor (which is destroyed).
		  if (lid=='submit') {
		    e.editor.container.getAscendant("form", true).removeListener(lid,e.editor.ckgeshi.listeners[lid]);
		  }
		  e.editor.removeListener(lid,e.editor.ckgeshi.listeners[lid]);
		  console.log('Listener removed: '+lid);
		}
		
		delete e.editor.ckgeshi.options;
		e.removeListener();
		delete e.editor.ckgeshi;
		delete iCKGeshi; // toDo: Verify this does not cause problems w/ ajax-submitted-forms on a page with multiple ckeditor_Geshi enabled forms.
	  },editor.ckgeshi));
    }
});

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~    CKGeshi Interface     ~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function iCKGeshi(editor,name,path,_volatiles) {
  this.editor =  editor;
  this.path =    path;
  this.command = this.editor.addCommand(this.name=name,this);
  this.command.modes= {wysiwyg:1, source:0}; // button should only work in wysiwyg mode
  this.command.canUndo =   false;   // disabled undo redo functionality for button events. Todo: should functionality be added?
  this.preProcessNeeded =  true;    // flag: permits execution of data preProcessing; so that dataProcessor rules filtrate properly
  this.postProcessNeeded = false;   // flag: is for reversing the effects of preProcess for thereafter usage in drupal
  this.selectionIsCode =   false;   // flag: used in the selectionChange event handler (.refresh() below) to determine contextual situations for buttons & their availability
  
  this.enabled_buttons_def = (_volatiles.enabled_buttons_def);
  this.tabKeyText =          (_volatiles.tabKeyText);
  this.available_languages = (_volatiles.available_languages);
  this.enterTypes = {};
  this.enterTypes[CKEDITOR.ENTER_P] = 'p';
  this.enterTypes[CKEDITOR.ENTER_BR] = 'br';
  this.enterTypes[CKEDITOR.ENTER_DIV] = 'div';
  
  // update button state, such that we can visibly detect when we are working(selecting) in a code-block/region
  // and so that our dialog can operate properly with regard to the active selection, at all times.
  this.refresh = function(data) {
    if (!data) return;
    var el = data.getStartElement();
    var isCodeArea = (el && el.getName().indexOf(this.editor.ckgeshi.codearea.type) !== -1);
    var hasCodeDefinition = (isCodeArea && (el.getAttribute("language")));
    this.setState(hasCodeDefinition? CKEDITOR.TRISTATE_ON : CKEDITOR.TRISTATE_OFF );
    this.editor.ckgeshi.selectionIsCode = hasCodeDefinition;
    // and toggle any buttons based on current selection result.
    for (cmd in this.blocked_commands) 
      if (this.selectionIsCode) this.blocked_commands[cmd].disable();
      else this.blocked_commands[cmd].enable();
  }
  this.onKey = function(code) {
    var selection = this.editor.getSelection();
    var parent = selection.getCommonAncestor().getAscendant(this.editor.ckgeshi.codearea.type, true);
    var range =  selection.getRanges()[0]; // toDo: Verify operability in firefox when multiple nodes (non-text) exist within selection.
    
    if (code === 13 || code === (CKEDITOR.SHIFT+13)) {
      this.cancel();this.stop();
      if (range.startOffset !== range.endOffset) return;
      // DEPRECATED:: this.editor.insertText(spacer); // can cause malformations to <pre> element by inserting html tags due to ckeditor defaults on receiving \n (br) or \n\n (p) .
      var spacer = (code===13)? '\n' : '\n\n'; // allow user to submit double-insertion if shift key is used
      var contents = parent.getText();
      
      if (range.checkStartOfBlock()) {
        contents = (spacer + contents);
      }
      else if ((contents.length - range.startOffset) <= 1) {
        contents = (contents + spacer);
      }
      else {
        var postCaretContents = contents.substring(range.startOffset, contents.length);
        var preCaretContents = contents.substring(0, range.startOffset);
        contents = (preCaretContents + spacer +postCaretContents);
      }
      parent.setText(contents);
    } else if (code === 9) {
      // single-line indent
      if (range.startOffset === range.endOffset) {
        this.editor.insertText(this.editor.ckgeshi.tabKeyText);
      }
      // multi-line indent
      else {
        var contents = parent.getText();
        var regex = new RegExp('^(.*)','gm');
        var selection_contents = contents.substring(range.startOffset,range.endOffset);
        while (match = regex.exec(selection_contents)) {
          contents = contents.replace(match[0], this.editor.ckgeshi.tabKeyText + match[0]);
        }
		delete regex;
        parent.setText(contents);
      }
    } else if (code === (CKEDITOR.SHIFT+9)) {
      var contents = parent.getText();
      
      var start = contents.lastIndexOf('\n', range.startOffset - 1) + 1;
      var end =   contents.indexOf('\n', range.startOffset);
      var selection_contents = (contents.substring(start, end));
      var regex = new RegExp('^(' + (String.fromCharCode(160)) + '|' + (String.fromCharCode(32)) + '{1,' + this.editor.ckgeshi.tabKeyText.length + '})+([\w\W]*)', 'm');
      
      // single line Unindent..
      if (range.startOffset === range.endOffset || selection_contents.indexOf('\n') !== -1) {
        var match = regex.exec(selection_contents);
        if (match === null || match.length < 1 || (match[0] === 'undefined' || match[0] === null)) return;
        var result = selection_contents.replace(match[0], '');
        contents=contents.replace(selection_contents, result);
        range.endOffset = (range.startOffset=range.startOffset - this.editor.ckgeshi.tabKeyText.length);
      } 
      // multi-line Unindent..
      // toDo: Rewrite to support eol detection; so that .replace operations are performed per-line -- thus allowing trailing "tab" spaces to exist concurrently (so that ONLY the tab-spaces @ front of line are replaced, not @ eol or within)
      else if ( (selection_contents = contents.substring(range.startOffset, range.endOffset)).indexOf('\n') !== -1) {
        var result = selection_contents;
        var match = regex.exec(result);
        // ensure we have an initial match. Otherwise, there is NOT one present within selection, and we should exit event now.
		if (match === null || match.length < 1 || (match[0] === 'undefined' || match[0] === null)) return;
        var lenDecr = (this.editor.ckgeshi.tabKeyText.length);
        do {
          result = result.replace(match[0], '');
          lenDecr += (this.editor.ckgeshi.tabKeyText.length);
        } while (match = regex.exec(result));
        contents = contents.replace(selection_contents, result);
        
        range.startOffset = (range.startOffset - lenDecr);
        range.endOffset   = (range.endOffset - lenDecr);
      }
      delete regex;
      parent.setText(contents);
      console.log('[onKey]: ToDo. Range selection invalid when @ line start. Currently contains: ');
      console.log(range);
      selection.selectRanges(range); // ToDo: this doesn't work as expected. investigate & resolve
    }
  }
  
  
  this.codearea={
    type: (_volatiles.inputType), // this is the container used while editing in WYSIWYG mode in ckeditor. It is converted to .safeType when outputting raw data. (Because <pre>, while properly handling wysiwyg mode, when highlighted by geshi will cause malformations to line numbering)
    style: (_volatiles.inputStyle),
    // for usage in [pre|post]Process only. Should be a container tag that will not adversely affect the geshi filter line numbering. May differ depending on theme.
      safeType: (_volatiles.outputType),
      safeStyle: 'display: inline;', // ToDo: this is not currently used. Is it desired?
    // --------------------------
    codedef: (_volatiles.codedef),
    
    
    OnInput: function(element) {
      for (var index in GFT_SCAN_ORDER) {
        var id = GFT_SCAN_ORDER[index];
        var cdef = this.codearea.codedef[id];
        if (!cdef) continue; // skip if tagStyle is not available.
      
        var value = element.children[0].value;
        console.log('[onInput]: entering codedef_to_element matcher w/ input:');
        console.log({'value': value, 'test_contains': value.indexOf(cdef.pre), 'codedef.pre': cdef.pre});
        // if the code-region being scanned contains a code-definition...
        if (value.indexOf(cdef.pre) !== -1) {
          var regex=new RegExp(cdef.rgx_pre + '(' + ((cdef.post.indexOf('&lt;') !== -1)? '.*' : '[^'+cdef.rgx_post+']+|.*') + ')' + cdef.rgx_post, 'g');
          var match;
          console.log('[onInput]: matches_loop will use:');
          console.log({r:regex,sz:value});
          while ((match = regex.exec(value)) !== null) { // for every available code-definition
            var opts_entry = match[1];
            if (opts_entry.indexOf('language') === -1) {
              console.log('[onInput_matchLoop]: skipping entry, not for geshi: ' + opts_entry);
              continue; // skip <code> entries that are not for geshi highlighting.
            } else console.log('[onInput_matchLoop]: located entry, converting it to element now: ' + opts_entry);
            var attr_regex = /(\S+)=["']?((?:.(?!["']?\s+(?:\S+)=|[>"']))+.)["']?/g; // match all pairs of: var="value"
            var attr_match;
            while ((attr_match = attr_regex.exec(opts_entry)) !== null) { // for every attribute in code-definition
              if (typeof this.options[attr_match[1]] !== 'undefined') {
                var attr_value = attr_match[2];
                if (attr_value.replace('"', '').length === 1) {
                  // toDo: the following "hack" is currently used to prevent problem in the above regex, where items of length=1 obtain a prefixed extra-quotation mark(fatal).
                  attr_value = (attr_value.replace('"', ''));
                  console.log("[onInput_attributeLoop]: regex_hackfix(" + attr_match[1] + "):: [" + attr_value + "]");
                }
                console.log("\t" + attr_match[1] + "=[" + attr_value + "]");
                element.attributes[attr_match[1]] = attr_value;
              }
              else console.log("\tNo such attribute: [" + attr_match[1] + "]. Discarding.");
            }
            // finally store the cdef type for later recall.
            element.attributes['name'] = id; // usage of 'name' attribute is without purpose or consequence, merely for storage.
            
            // remove the code-definition since the element now has the attributes stored
            value = (value)
              .replace(match[0], '')
              .replace((cdef.end.indexOf("&lt;") !== -1)? new RegExp(cdef.end + '$') : cdef.end, '');
            element.children[0].value = value;
            element.attributes.style = this.codearea.style; // and give it the style sent from drupal.
          }
		  delete regex;
        }
        else if (!element.attributes["language"]) { // skip if it has dynamic codedef (duplicate-event execution)
          // this occurs when a codearea.type(eg <pre>) contains no code-def; and should be assumed as normal element not a code-def.
          // ToDo: handle potential corrupted/modified entry, via regex etc, in attempt to fix it?
        }
      }
    },
    OnOutput: function(element) {
      if (element.attributes["language"]) {
        delete element.attributes.style;
        
        var cid = parseInt(element.attributes['name']); // store the code-definition-type ID
        delete element.attributes['name']; // and remove it as it is merely for internal usage of this plugin
        var codedef = '';
        for (var attr in element.attributes) {
          if (typeof this.options[attr] ==='undefined') continue; // skip any attributes that aren't a part of the ckgeshi options.
          var attr_value = element.attributes[attr];
          if (typeof element.attributes[attr] === 'string') attr_value = ('"' + attr_value + '"');
          codedef += (((codedef.length < 1)? '' : ' ') + attr + '=' + attr_value);
          delete element.attributes[attr];
        }
        codedef = (this.codearea.codedef[cid]).pre + this.options.parseRaw(codedef) + (this.codearea.codedef[cid]).post;
        element.children[0].value = (codedef+element.children[0].value + (this.codearea.codedef[cid]).end);
      }
    },
    
    preProcess: function(evdata) { // performed on initial load of wysiwyg mode, and when switching to rich text editor mode (thus wysiwyg mode)
      console.log('[preProcess] scanning for any code definitions...');
      for (var index in GFT_SCAN_ORDER) {
        var id = GFT_SCAN_ORDER[index];
        var def = this.codedef[id];
        if (!def) continue; // skip if tagStyle is not available.
        
        if (evdata.dataValue.indexOf(def.pre) !== -1) {
          console.log('[preProcess] Located code-def of tagStyle: ' + def.pre + " ... " + def.post);
          evdata.dataValue=this.rgxSwitcheroo(evdata.dataValue, def, this.safeType, this.type);
		  
		  // custom handler for '<cdef>' style tags
          if (def.pre.indexOf('<') !== -1) {
            var start;
            while ((start = evdata.dataValue.indexOf(def.pre)) !== -1) {
              var codedef = evdata.dataValue.substring(start, evdata.dataValue.indexOf(def.post, start) + 1); // todo: shouldn't this use lastIndexOf?
              if (codedef.indexOf('language') === -1) continue; // skip html code entries that are not to be geshi highlighted. // ToDo: Test what happens when someone nests a geshi code-region inside of an html code-region via source edit mode.
              console.log("[preProcess_typeANGLE] code-def identified as valid: " + codedef);
              evdata.dataValue = evdata.dataValue
                .replace(codedef, codedef.replace("<", "&lt;").replace('>', '&gt;'))
                .replace(def.end, def.end.replace("<", "&lt;").replace('>', '&gt;'));
            }
            def.pre =  def.pre.replace('<', '&lt;');
            def.post = def.post.replace('>', '&gt;');
            def.end =  def.end.replace('<', '&lt;').replace('>', '&gt;');
            def.rgx_pre  = def.rgx_pre.replace('<', '&lt;');
            def.rgx_post = def.rgx_post.replace('>', '&gt;');
            def.rgx_end  = def.rgx_end.replace('<', '&lt;').replace('>', '&gt;');
          }
        }
      }
      console.log('--------------------------------------');
    },
    postProcess: function(evdata) { // performed on submit and on switch to plain text-editor:
      console.log('[postProcess] scanning for any code definitions...');
      for (var index in GFT_SCAN_ORDER) {
        var id = GFT_SCAN_ORDER[index];
        var def = this.codedef[id];
        if (!def) continue; // skip if tagStyle is not available.
        console.log('[postProcess]::SCAN_ORDER['+id+']');
		
        if (evdata.dataValue.indexOf(def.pre) !== -1) {
          evdata.dataValue = this.rgxSwitcheroo(evdata.dataValue, def, this.type, this.safeType);
          if (def.pre.indexOf('&lt;') !== -1) {
		    console.log('[postProcess]:: Type_Angle on SCAN_ORDER: '+id);
            var start;
            while ((start = evdata.dataValue.indexOf(def.pre)) !== -1) {
              var codedef = evdata.dataValue.substring(start, evdata.dataValue.indexOf(def.post, start) + 4);
              console.log("[postProcess_typeANGLE] code-def identified as valid: " + codedef);
			  evdata.dataValue = evdata.dataValue
                .replace(codedef, codedef.replace('&lt;', '<').replace('&gt;', '>'))
                .replace(def.end, def.end.replace('&lt;', '<').replace('&gt;', '>'));
            }
            // if user switches to plain editor and back to rich, the following becomes important:
            def.pre  = def.pre.replace('&lt;', '<');
            def.post = def.post.replace('&gt;', '>');
            def.end  = def.end.replace('&lt;', '<').replace('&gt;', '>');
            def.rgx_pre  = def.rgx_pre.replace('&lt;', '<');
            def.rgx_post = def.rgx_post.replace('&gt;', '>');
            def.rgx_end  = def.rgx_end.replace('&lt;', '<').replace('&gt;', '>');
          }
        }
      }
      console.log('--------------------------------------');
    },
    // used by [pre|post]Process in order to swap the editor tag w/ the safe tag for output.
    // This must be done to preserve formatting in the editor.
    rgxSwitcheroo: function(data,def,tag,tag_r) {
      var regex = new RegExp("<" + tag + "[\\s\\S]{1,10}?" + def.rgx_pre + "[\\s\\S]*?" + def.rgx_end + "[\s\S]?</" + tag, "g");
      console.log('[switcheroo] attempting to swap all cdef wrappers, with type: ' + def.pre+'...' + def.post+'  in..');
      console.log({d: data, rgx: regex});
      var match;
      while (match = regex.exec(data)) for (vid in match) {
        if (vid === "index" || vid === "input") continue;
        match[vid] = match[vid].substring(1, match[vid].length); // rid the following replacement mechanism of the prefixed '<' which was used merely for detection purposes.
        console.log('\tMatched entry: ');
        console.log({'match': match, 'vid': vid});
        var rgx_replace = new RegExp("(?:^|\w)" + tag + "?|" + tag + "?$", "g");
        var replace = match[vid].replace(rgx_replace, tag_r);
        console.log('\tTag-Match replace: ');
        console.log({'rgx_replace': rgx_replace,'replace': replace});
        data = data.replace(match[vid], replace);
      }
	  delete regex;
      return data;
    }
  };

  IO=function() {
    this.language;
    this.linenumbers;
    this.fancy;
    this.start;
    this.title;
    // Notice: do not add non-function objects to this 'class'; the logic of "options[an_option] != undefined" , relies upon it and WILL otherwise break.
    this.reset=function() {
      this.language =    "html";
      this.linenumbers = null;
      this.fancy =       null;
      this.start =       null;
      this.title =       null;
    };
	this.reset();
  }
  IO.prototype={
    // read the selection for dialog, and save it's output.
    // These are directly executed by the 'dialogs/ckgeshi.js' .onShow and .onOk events.
    // Note that individual _validation_ of options('this') to/from dialog are handled by their respective .setup and .commit events.
    selectionInput: function(editor) {
      var container_type = editor.ckgeshi.codearea.type; // generally a "pre"
      var container = editor.getSelection().getStartElement().getAscendant(container_type, true);
      
      if (!container) editor.ckgeshi.options.reset(); // use defaults for new entry
      else {
        console.log('[selectionInput] Found existing code-def container, reading for dialog...');
        for (opt in editor.ckgeshi.options) {
          if (typeof editor.ckgeshi.options[opt] === 'function') continue;
          var attr_value = container.getAttribute(opt);
          if (typeof attr_value !== 'undefined' && attr_value !== null) { // skip options not present in the container's attributes
            editor.ckgeshi.options[opt] = attr_value;
            console.log("input read: " + attr_value);
          }
        }
      }
    },
    selectionOutput: function(editor) {
      var container_type = editor.ckgeshi.codearea.type;
      var parent = editor.getSelection().getStartElement();
      var container = parent.getAscendant(container_type, true);
      
      if (!container) { // prepare the new container for code-region
        console.log("[selectionOutput]: Generating new container for code-region, using parent: ");
        console.log({element: parent});
        container = new CKEDITOR.dom.element(container_type);
        container.setText("..insert your code here..");
        container.insertBefore(parent);
        container.setAttribute('style', editor.ckgeshi.codearea.style);
      } else console.log('[selectionOutput]: Processing dialog options and assigning them to element for storage: ');
      for (opt in editor.ckgeshi.options) {
        if (typeof editor.ckgeshi.options[opt] ==='function') continue;
        var attr_old = container.getAttribute(opt);
        var attr_new = editor.ckgeshi.options[opt];
        
        if (typeof attr_new !== 'undefined' && attr_new !== null) {
          if (attr_old === attr_new) continue;
          console.log("\tSetting attribute(" + opt + ")::   old[" + attr_old + "], new[" + attr_new + "]");
          container.setAttribute(opt, attr_new);
        } else if (attr_old !== null) {
          container.removeAttribute(opt);
          console.log("\tRemoved attribute(" + opt + ")[" + typeof(attr_old) + "]: " + attr_old);
        } // else: attribute is invalid, and isn't currently assigned. So do nothing.
      }
    },

    // used by .onOutput callback event for codarea.type. // ToDo: verify whether this is needed; as geshifilter.pages.inc now contains the filters necessary for proper display output of highlighting
    parseRaw: function(rawHTML) {
      return rawHTML
        .replace(/<br>/g, '\n')
        .replace(/&amp;/g, '&')
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&quot;/g, '"');
    }
	
  } // END: IO (options) definition
  this.options = new IO(); // generate default options for the current instance. // These are overwritten when selection changes; therefore this is only for initialization purposes for logic checks herein.
} // END: ckgeshi Interface definition
})(jQuery); // END: CKEditor Plugin definition