<?php
/**
 * @file
 * Admin page for configuring the parameters sent to the
 * ckgeshi plugin used by CKEditor.
 */

// Include file for the default options for this module.
require_once drupal_get_path('module', 'ckeditor_geshi') . '/ckeditor_geshi_default_options.inc';

/**
 * Implements hook_admin_form().
 */
function ckeditor_geshi_admin_form($form_state) {
  // Use default options, by default.
  $options = variable_get('ckeditor_geshi_vars', ckeditor_geshi_defaultOptions());
  // Setup links used by form page.
  $geshi_settings = t(l(t('GeSHi Filter settings'), 'admin/config/content/formats/geshifilter'));
  $geshi_language_settings = t(l(t('GeSHi Filter Language settings'), 'admin/config/content/formats/geshifilter/languages'));
  $format_profiles = t(l(t('Text Formats'), 'admin/config/content/formats') . ' and ' . l(t('CKEditor Profiles'), 'admin/config/content/ckeditor'));

  if (geshifilter_use_format_specific_options()) {
    $tagstyles_output = '<li>' . t('Format-Specific TagStyles') . ': <i>' . t('integrated by CKGeshi plugin; configured individually by respective') . ' ' . $format_profiles . '.</i>';
  }
  else {
    $tagstyles_output = '<li><b>' . t('Global TagStyles') . '</b>: <i>' . t('integrated by CKGeshi plugin; made globally-available via the') . ' ' . $geshi_settings . '.</i><ul style="list-style-type: none;">';
    // Get array containing the geshifitler tags enabled.
    $tags = explode(' ', geshifilter_tags(NULL));
    // Get tag styles to be used for said tags.
    $tag_styles = _geshifilter_tag_styles(NULL);
    // Remove PHPBlock style, as it is not supported for security reasons.
    // ToDo: Verify this is necessary.
    unset($tag_styles[GESHIFILTER_BRACKETS_PHPBLOCK]);
    foreach ($tag_styles as $type => &$value) {
      // If the styleType equals it's value (ie non-zero) then it is enabled...
      if ($type == $value) {
        // Generate output for it on the page for reference purposes.
        switch ($type) {
          case GESHIFILTER_BRACKETS_ANGLE:
            $value = ckeditor_geshi_admin_formulateTagStyle('&lt;', $tags, '&gt;');
            break;
          case GESHIFILTER_BRACKETS_SQUARE:
            $value = ckeditor_geshi_admin_formulateTagStyle('[', $tags, ']');
            break;
          case GESHIFILTER_BRACKETS_DOUBLESQUARE:
            $value = ckeditor_geshi_admin_formulateTagStyle('[[', $tags, ']]');
            break;
        }
        $tagstyles_output .= ('<li>' . t($value) . '</li>');
      }
    }
  }

  $form['info'] = array('#markup' =>
    '<div style="border: groove 2px gray;padding: 6px;10px;background-color: lightblue;"><ul style="list-style-type:square;">' .
    '<lh style="font-weight: bold;">' . t('Brief Configuration Overview') . ':</lh>' .
    '<li>' . t('Available Languages') . ': <i>' . t('provided automatically, and configured in the') . ' ' . $geshi_language_settings . '.</i></li>' .
    $tagstyles_output . '</ul></li></ul></div>' .
    '<br /><hr><br /><p style="display:inline;margin: 1em; font-weight:bold;">' . t('Use this page to control the ckeditor_geshi plugin (integration of GeSHi Filter into CKEditor).') . '</p><br />' .
    '</div><br /><br />',
  );

  $form['ckeditor_geshi_general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
  );
  $form['ckeditor_geshi_general']['useDebug'] = array(
    '#type' => 'select',
    '#title' => t('Javascript-Plugin Mode'),
    '#default_value' => $options['useDebug'] ? "Debug" : "Optimized",
    '#description' => t('Select either: <ul><li><b>Optimized</b> (compressed version; 1 file @~15kb): <i>minimal resource load</i>;</li><li><b>Debug</b> (documented version; 3 files @~41kb): <i>outputs debug info to console</i></li></ul>'),
    '#options' => drupal_map_assoc(array("Optimized", "Debug")),
    '#required' => TRUE,
  );
  $form['ckeditor_geshi_general']['tabKeySize'] = array(
    '#type' => 'select',
    '#title' => t('Tab-key Spacing'),
    '#default_value' => $options['tabKeySize'],
    '#description' => t('How many spaces should be used to form an equivalent tab-spacing? Default: 4 spaces == 1 tab.'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 8, 10)),
    '#required' => TRUE,
  );

  $buttons = array_keys($options['enabled_buttons_def']);
  $sz_buttons = implode(',' , $buttons);
  $form['ckeditor_geshi_general']['enabled_buttons_def'] = array(
    '#type'  => 'textfield',
    '#title' => t('Enabled CKEditor Buttons'),
    '#default_value' => $sz_buttons,
    '#description' =>
    t('Which buttons should be <b>allowed</b> when editing a code-region. Comma-seperated button command names.<br />') .
    t("<b>Warning</b>: Alter with <u>caution</u>! Invalid entries could break the loading of CKEditor in javascript."),
    '#size'      => 62,
    '#maxlength' => 165,
    '#required'  => TRUE,
  );

  $form['ckeditor_geshi_io'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Input/Output Modifiers'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $form['ckeditor_geshi_io']['pre_caution'] = array('#markup' =>
    '<div style="border: groove 2px gray;padding: 6px;4px;">',
  );
  $form['ckeditor_geshi_io']['inputType'] = array(
    '#type'  => 'textfield',
    '#title' => t('Input Wrapper'),
    '#default_value' => $options['inputType'],
    '#description' => t('This is by default a &lt;pre&gt; tag, since it will preserve all formatting. This really should not be changed.'),
    '#size'       => 4,
    '#maxlength'  => 12,
    '#disabled'   => TRUE,
    '#required'   => TRUE,
    '#attributes' => array('id' => 'ckeditor_geshi_inputType'),
  );
  $form['ckeditor_geshi_io']['inputType_enable'] = array(
    '#type'  => 'checkbox',
    '#title' => t('I understand the risk, and wish to change this tag anyway.'),
    '#description' => t('Non-&lt;pre&gt; tags will cause malformations to formatting of code within ckeditor, potentially if not absolutely breaking the plugin, & the code it attempts to parse via regular expressions.'),
    '#default_value' => FALSE,
    '#attributes' => array(
      'onclick' => 'var o=document.getElementById(\'ckeditor_geshi_inputType\');o.disabled=!this.checked;o.focus();',
    ),
  );
  $form['ckeditor_geshi_io']['post_caution'] = array('#markup' => '</div>');

  $path = base_path() . drupal_get_path('module', 'ckeditor_geshi') . '/plugins/' . (($options['useDebug']) ? 'ckgeshi_debug/' : 'ckgeshi/');
  $form['ckeditor_geshi_io']['inputStyle'] = array(
    '#type'  => 'textarea',
    '#title' => t('Input Wrapper Style'),
    '#default_value' => htmlspecialchars_decode($options['inputStyle']),
    '#description' =>
    t('The style to be used on the above <b>Input Wrapper</b> tag in CKEditor, via inline CSS. <br />') .
    t('<b>Note</b>: You may use the term <u>[!path]</u> to automatically insert the plugin path (eg. for images): <i>_[!path]</i> <br />',array('_[!path]' => $path)) .
    t('<br />The preview style will update automatically on-screen, when you tab-out or click-out of input area (when it loses focus), so that you may see what will appear within CKEditor.'),
    '#rows'       => 5,
    '#required'   => FALSE,
    '#wysiwyg'    => FALSE,
    '#attributes' => array(
      'style'  => htmlspecialchars_decode(str_replace('[!path]', $path, $options['inputStyle'])),
      'onblur' => 'var previewStyle=this.value.replace("[!path]","' . $path . '");this.setAttribute("style",previewStyle);this.style.cssText=previewStyle;',
    ),
  );
  $form['ckeditor_geshi_io']['outputType'] = array(
    '#type'  => 'textfield',
    '#title' => t('Output Wrapper'),
    '#default_value' => $options['outputType'],
    '#description' => t('By default a &lt;div&gt; tag; this is the wrapper surrounding the geshi highlighted code-region.<br />We <b>must</b> use an output wrapper so that ckeditor can preprocess our codearea - regardless of tag-style used. <br /><b>Note</b>: This tag WILL be present on rendered output of highlighted code, as a wrapper. <br /><b>Note</b>: &lt;pre&gt; tag causes malformations to line-numbering, and is not recommended.'),
    '#size'      => 4,
    '#maxlength' => 12,
    '#required'  => TRUE,
  );


  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  $form['restore_perform'] = array(
    '#type'  => 'hidden',
    '#value' => 0,
    '#attributes' => array('id' => 'restore_perform'),
  );
  $form['restore'] = array(
    '#type' => 'submit',
    '#value' => 'Restore Defaults',
    '#executes_submit_callback' => FALSE,
    '#attributes' => array(
      'id' => 'restore_defaults',
      'onclick' => 'if (confirm(\'' . t('Are you certain you wish to reset ALL settings to their defaults?') . '\')) {' .
      'document.getElementById(\'restore_perform\').value=1; document.forms[0].submit();} else return false;',
    ),
  );
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function ckeditor_geshi_admin_form_submit($form, &$form_state) {
  // If user clicked the Reset To Defaults button...
  if (intval($form_state['input']["restore_perform"]) == 1) {
    // Load from the 'defaults' include file.
    variable_set('ckeditor_geshi_vars', ckeditor_geshi_defaultOptions());
    drupal_set_message(t('The ckeditor_geshi module settings have been reset to their defaults.'));
  }
  else {
    $buttons = array();
    $buttons_list = str_replace(' ', '', $form_state['values']['enabled_buttons_def']);
    $temp_buttons = explode(',', check_plain($buttons_list));
    foreach ($temp_buttons as $id => $button) {
      $buttons[$button] = 0;
    }

    $useDebug = (check_plain($form_state['values']['useDebug']) == "Debug") ? TRUE : FALSE;
    // Construct the options array to store in database.
    // Note: Very similair to array dispatched by .module file
    // to javascript plugin for CKEditor.
    $options = array(
      'enabled_buttons_def' => $buttons,
      'useDebug' => $useDebug,
      'tabKeySize' => intval($form_state['values']['tabKeySize']),
      'inputType'  => check_plain($form_state['values']['inputType']),
      'inputStyle' => check_plain($form_state['values']['inputStyle']),
      'outputType' => check_plain($form_state['values']['outputType']),
    );
    variable_set('ckeditor_geshi_vars', $options);
    drupal_set_message(t('The configuration options have been saved.'));
  }
}

/**
 * Generate the format:  <<pre_style>> tagName <<postStyle>>.
 * Eg::  <code> or <blockcode> etc.
 *
 * Merely for visual output on screen.
 * Not influenced by user input -- fed from GeSHi Filter tags.
 *
 * @param string $pre
 *   the prefix to the potential tag
 * @param array $a_tags
 *   an 0-based array containing strings that are available tags.
 * @param string $post
 *   the postfix on the potential tag
 *
 * @return string
 *   a composited string containing all possible tag styles given a tag context.
 */
function ckeditor_geshi_admin_formulateTagStyle($pre, $a_tags, $post) {
  $sz_tagstyle = '';
  foreach ($a_tags as $id => $tag) {
    $sz_tagstyle .= ($pre . $tag . $post);
    if (($id + 1) < count($a_tags)) {
      $sz_tagstyle .= (', ');
    }
  }
  return $sz_tagstyle;
}
