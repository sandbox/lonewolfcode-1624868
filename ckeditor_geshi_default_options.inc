<?php
/**
 * @file
 * The default options used by this module.
 */

require_once drupal_get_path('module', 'geshifilter') . '/geshifilter.inc';

/**
 * Provides this module the default options used for ckgeshi plugin.
 * @return array
 *   an array containing the default options fed to ckgeshi plugin.
 */
function ckeditor_geshi_defaultOptions() {
  return array(
    // Buttons that should be allowed when editing source code in wysiwyg.
	// --------------------------------------------------------------
    // Note that the usage of assigning it to 0 (Zero) is unimportant.
    // It is merely an associative array that has defined text keys that
    // represent(directly) the button command used in CKEditor toolbar.
    'enabled_buttons_def' => array(
      // Enable the button to switch between wysiwyg mode and source mode.
      'source'  => 0,
      // Enable insertion of special characters.
      'specialchar' => 0,
      // Enable miscellaneous buttons that cooperate w/ plugin.
      'undo'    => 0,
      'redo'    => 0,
      'find'    => 0,
      'replace' => 0,
      'cut'     => 0,
      'copy'    => 0,
      'paste'   => 0,
      // Enable the paste-text gui button.
      'pastetext' => 0,
      // Enable context-menu operations (important for feature of this module).
      'contextMenu' => 0,
    ),
    // By default use 4 spaces for a single tab.
    'tabKeySize' => 4,
    // By default use a <pre> tag for maintaining formatting within
    // the WYSIWYG mode of CKEditor.
    'inputType'  => 'pre',
    // A default style seen within CKEditor. Notice the use of '[!path]' which
    // inserts the path to the plugin folder - for use with images.
    'inputStyle' => 'margin-top: 0; margin-left: 1em; padding: 0.5em; border: 3px groove; background-color: #F8F8F8;line-height: 1.3;' .
    'background-image: url("[!path]images/ckgeshi_bg.png"); background-size: 90px 90px; background-repeat: repeat; background-attachment: fixed;',
    // By default use a <div> tag for an output wrapper, which will be used on
    // subsequent preprocessing when loading CKEditor, as well as for providing
    // control over output in future release.
    'outputType' => 'div',
    // This is the GeshiFilter module's Tag Style type, as an integer. These are
    // defined by GeshiFilter module in it's .module file. This should be left 
    // alone -- it is for feeding the "ckgeshi" Javascript plugin the php data.
    'tagStyle'   => NULL,
    // Used by admin-page to switch between Optimized and Debug javascript code.
    'useDebug'   => FALSE,
  );
}
