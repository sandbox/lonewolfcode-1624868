A javascript-plugin for CKEditor, enhancing it to incorporate support for the GeSHi Filter module.

This module provides the included "ckgeshi" javascript-plugin the current settings from GeSHi Filter, so 
that CKEditor can create code-regions that will be highlighted by GeSHi Filter module. It supports 
both WYSiWYG mode and Source mode of CKEditor.

Sure you can switch to Plain-Text mode (exit CKEditor) or use your own Input Format without using CKEditor.
However this module enables CKEditor's WYSIWYG and Source modes to interact with GeSHi code-regions properly.


See the Project Page for screenshots: 
  http://drupal.org/sandbox/lonewolfcode/1624868

  

MINI-VERSION (default)
    The default plugin.js is a minified/compressed version of the plugin - for speed & caching purposes.
    './plugin/ckgeshi/source.plugin.js' can be renamed to replace 'plugin.js' in the case that you want
    to use the "source version" that outputs debug information, and has documentation.
	



CONTACT
  Author: lonewolfcode
  http://drupal.org/sandbox/lonewolfcode/1624868

	
REQUIREMENTS:
	http://drupal.org/project/ckeditor
	http://drupal.org/project/geshifilter
	
	
CREDITS:
	The following individuals/projects, were instrumental in the development of this module:
	Peter Petrik: http://peterpetrik.com/blog/ckeditor-and-geshi-filter
	PkLab: http://www.pklab.net/index.php?id=350
	Reviewers & Testers that made this possible: KrisBulman, rrbambrey, mitchell
	
	
INSTALLATION
*** See INSTALL.txt for detailed instructions; MUST be installed properly.
  
    1] Install & enable CKEditor or WYSIWYG, and configure to your liking
    2] Install & enable GeSHi Filter, and configure to your liking
    3] Install & enable this module. Please see the included INSTALL.txt for detailed instructions. 
    Simplified Instructions:
        1.) Text formats page: admin/config/content/formats/ 
           - ensure ckeditor_geshi filter is placed before GeSHi filter.
		Warning: If step #1 is not configured, code-regions saved by a form may become botched by CKEditor, & GeSHi Filter.
		
        2.) CKEditor administration page: /admin/config/content/ckeditor
           - enable the plugin for your profile, and drag the  button to your toolbar.
    
	    3.) Review the DETAILED instructions(INSTALL.txt), to ensure you have properly configured everything.
    
  Afterwards you will have a GeSHi button on your toolbar in CKEditor, and the plugin will 
  format code areas you specify for GeSHi Filter to highlight.

  You can change the style of code-areas, and other settings, at the ckeditor_geshi module's admin page 
  under Content Authoring: 
     /admin/config/content/ckeditor_geshi

	
FEATURES
    * GUI for inserting new and modifying existing Code-Regions that will be highlighted by GeSHi Filter
    * Dynamically obtains available languages based upon GeSHi Filter settings (configured by GeSHi Filter module).
    * Support for both Global Tag Styles, and per-input-format Tag Styles (configured by GeSHi Filter module).
    * Support all current features of GeSHi Filter module: Title, Line-Numbering options, Fancy highlighting
    * Admin page permitting customization of Input/Output tags, and their styles.
    * Single-line and Multi-line indent/unindent (spacing is configurable) inside CKEditor, Enter / Shift+Enter key support, 
	  and paste support. Copy is on the to-do list. :)
    * Context Menu used to adjust the code-region with respect to it's surrounding elements in CKEditor WYSiWYG mode.
    * Configurable disabling/enabling of specific buttons while inside a "Code-Region" in CKEditor WYSiWYG mode.
	* Support for AJAX forms with CKEditor enabled

	
CAVEATS:
	* Hooks the Drupal.ckeditorOff function provided by CKEditor, to add support for post-processing (see Issue Request: http://drupal.org/node/1624880). 
	  Potentially requires manual update if CKEditor changes this function dramatically in the future
	* Uses regular expressions, which by their nature, have the potential to fail when attempting to parse HTML. 
	  These will be exported to admin page, so that they can be tweaked as desired.
	

	
DESIGN / EXAMPLE : 
  Once this module has been installed and enabled (See INSTALLATION section), any form that uses the CKEditor GeSHi plugin will have 
  the GeSHi button on the toolbar, which you can use to insert new code-regions, & edit existing, that should be highlighted.

  In essence, it takes the following (as an example): 
    <div>
      <code language="cpp" title="some cpp">
        cout << "output" << endl;
      </code>
    </div>
  and temporarily (for WYSIWYG mode of CKEditor) converts it to:
    <pre language="cpp">
      cout << "output" << endl;
    </pre>

  Though this is an extremely oversimplified example (see the documented source code), it is the 
bare-foundation that permits CKEditor to properly edit and therefore output GeSHi code-regions that can be highlighted.

This therefore insinuates that you can use Source mode of CKEditor and type the above <div> code-region-definition
to produce an editable version inside the WYSIWYG mode of CKEditor. 
Of course, GeSHi Filter will highlight it regardless (assuming configured to do so) of if you use the WYSIWYG mode; since 
this module preProcesses all GeSHi Filter input (configurable).



TROUBLESHOOTING
   Important: 
	  Ensure you have viewed the INSTALL.txt file for the detailed installation instructions.
	  The CKEditor profile and Input Format filters MUST be configured _properly_.
   
   
  ---------------- Potential Problems / FAQ ----------------
   
   Q.] I have installed the plugin, and saved a form. GeSHi Filter appears to be working, but the saved output 
       is corrupted with html symbols, or isn't highlighted properly, or is missing portions of source-code.
	   --------------------------------
   >>  This means you did not follow INSTALL.txt; the 'ckeditor_geshi' filter MUST have a weight
       assignment BEFORE the 'GeSHi Filter' module's filter.
	   1.) You must fix the input format's filter weights according to INSTALL.txt, step 1.
	   2.) Then, manually edit the source code which is likely now littered with html-escape codes using 
	       the CKEditor's Source mode.
		   Your construct should match the example provided in the DESIGN/EXAMPLE section above.
           Note: It is safer to click the "switch to Plain-Text" href-button below the ckeditor instance for performing this fix.
   
   
   Q.] I do not see the multi-colored GeSHi button on the CKEditor toolbar.
       --------------------------------
   >>  See INSTALL.txt detailed instructions, step 4.





