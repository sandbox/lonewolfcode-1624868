<?php 
/**
 * @file
 * Module file containing form hook, and ckeditor/wysiwyg module hooks.
 *
 * Purpose is to provide the new ckgeshi plugin with the settings from
 * our admin page and from GeSHi Filter module.
 */

require_once drupal_get_path('module', 'ckeditor_geshi') . '/ckeditor_geshi_default_options.inc';


/**
 * Implements hook_ckeditor_plugin().
 */
function ckeditor_geshi_ckeditor_plugin() {
  return array('ckeditor_geshi' => array(
    'name' => 'ckgeshi',
    'desc' => 'CKEditor GeSHi - ' . t('A plugin to manage') . ' GeSHi ' . t('syntax highlighted code-regions inside') . ' CKEditor.',
    'path' => drupal_get_path('module', 'ckeditor_geshi') . '/plugins/ckgeshi/',
    ),
  );
}

/**
 * Implements hook_wysiwyg_plugin().
 */
function ckeditor_geshi_wysiwyg_plugin($editor, $version) {
  if ($editor == 'ckeditor') {
    return array('ckgeshi' => array(
      'path' => drupal_get_path('module', 'ckeditor_geshi') . '/plugins/ckgeshi/',
      'load' => TRUE,
      'extensions' => array('GeSHi' => 'CKEditor GeSHi'),
      ),
    );
  }
}

/**
 * Implements hook_menu().
 */
function ckeditor_geshi_menu() {
  $items['admin/config/content/ckeditor_geshi'] = array(
    'title' => 'CKEditor_GeSHi',
    'description' => 'Configure CKEditor_GeSHi.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ckeditor_geshi_admin_form'),
    'access callback' => 'ckeditor_geshi_admin_access',
    'file' => 'ckeditor_geshi.admin.inc',
  );
  return $items;
}


/**
 * If user admin can manage CKEditor, then they can enable the ckgeshi
 * plugin, and thus should be allowed to manage ckeditor_geshi module
 * which administers said plugin.
 */
function ckeditor_geshi_admin_access() {
  return module_exists('ckeditor') && user_access('administer ckeditor');
}


/**
 * Implements hook_filter_info().
 */
function ckeditor_geshi_filter_info() {
  $filters['ckeditor_geshi_filter'] = array(
    'title' => t("CKEditor_GeSHi - <em>Filter</em>"),
    'description' => t('Used alongside GeSHi Filter, for CKEditor filtration of code-areas. Must be placed <strong>before</strong> the GeSHi Filter in the <em>processing order</em>. Filters that process markup, such as HTMLTidy and WYSIWYG Filter, should always be performed <strong>after</strong> geshifilter, as they will malform code formatting that should remain constant.'),
    'process callback' => 'ckeditor_geshi_filter_process',
    'weight' => '-50',
  );
  return $filters;
}

/**
 * Process callback for the filter.
 * Will process the text that is about to be highlighted by GeSHi.
 */
function ckeditor_geshi_filter_process($text, $filter, $format, $langcode, $cache, $cache_id) {
  $codedef = array();
  $options = variable_get('ckeditor_geshi_vars', ckeditor_geshi_defaultOptions());

  $cdef_start = '[geshifilter-code';
  $cdef_end   = '[/geshifilter-code]';
  $start = 0;
  while ($start = strpos($text, $cdef_start, $start)) {
    $end = strpos($text, $cdef_end, $start) + drupal_strlen($cdef_end);
    $code_region = drupal_substr($text, $start, $end - $start);
    $new_data = str_replace("&gt;",     ">",  $code_region);
    $new_data = str_replace("&lt;",     "<",  $new_data);
    $new_data = str_replace("&lsaquo;", "�",  $new_data);
    $new_data = str_replace("&rsaquo;", "�",  $new_data);
    $new_data = str_replace("&#39;",    "'",  $new_data);
    $new_data = str_replace("&quot;",   "\"", $new_data);
    $new_data = str_replace("&amp;",    "&",  $new_data);
    $new_data = str_replace("&#10;",    "\n", $new_data);
    // $new_data = str_replace("&nbsp;",   " ", $new_data);.
    $text = str_replace($code_region, $new_data, $text);
    $start = $start + drupal_strlen($new_data);
  }
  return $text;
}


/**
 * Implements hook_form_alter().
 */
function ckeditor_geshi_form_alter(&$form, &$form_state) {
  $form['#after_build'][] = 'ckeditor_geshi_process_form';
}

/**
 * After Build function for form.
 * Inserts the variables for ckgeshi plugin, from the stored drupal variable.
 */
function ckeditor_geshi_process_form(&$form, &$form_state) {
  static $added = FALSE;
  // If plugin has not been loaded already into javascript output vars...
  if (!$added && ($js = drupal_add_js()) && isset($js['settings']['data'])) {
    // Obtain current settings as a merged array (multiple indexes exist).
    $settings = call_user_func_array('array_merge_recursive', $js['settings']['data']);

    // If the current form is indeed loading ckeditor...
    if (isset($settings['ckeditor']) || isset($settings['wysiwyg']['configs']['ckeditor'])) {
      $path_module = base_path() . drupal_get_path('module', 'ckeditor_geshi');
      $options = variable_get('ckeditor_geshi_vars', ckeditor_geshi_defaultOptions());

      // Setup path dynamically for swapping between Optimized & Debug code
      $path_plugin = $path_module . '/plugins/' . (($options['useDebug']) ? 'ckgeshi_debug/' : 'ckgeshi/');
      foreach ($settings['ckeditor']['input_formats'] as &$fmt) {
        if (isset($fmt['loadPlugins']['ckeditor_geshi'])) {
          $fmt['loadPlugins']['ckeditor_geshi']['path']=$path_plugin;
        }
      }

      // Insert the options necessary for the ckgeshi javascript plugin.
      ckeditor_geshi_options_phptojs($options, $path_plugin, $settings['ckeditor']['input_formats']);
      $options['module_path'] = $path_module;
      $options['path'] = $path_plugin;
      drupal_add_js(array('ckeditor_geshi' => $options), 'setting');
      drupal_add_js(array('ckeditor' => $settings['ckeditor']), 'setting');
      $added = TRUE;
    }
  }
  return $form;
}

/**
 * Convert options stored in drupal,
 * into options useable by javascript plugin in ckeditor.
 */
function ckeditor_geshi_options_phptojs(&$options, $path, $input_formats) {
  // In this case, each input text format has it's own specific options.
  // So we must load them into an array javascript can detect & process.
  if (geshifilter_use_format_specific_options() != 0) {
    $options['formats'] = array();
    $keys = array_keys($input_formats);
    foreach ($keys as $id => $fmt) {
      $options['formats'][$fmt] = array();
      ckeditor_geshi_getCodeDef_byFormat($fmt, $options['formats'][$fmt]);
    }
  }
  // Otherwise, we can simply load the global options.
  else {
    $options['codedef'] = array();
    // Configured to automatically update the settings if tag or tagStyle
    // has been changed in Geshi Filter module and not updated in
    // the ckeditor_geshi module.
    ckeditor_geshi_getCodeDef_byFormat(NULL, $options['codedef']);
  }

  // Insert the button created by our plugin, since it is, naturally,
  // required to be enabled when editing geshi code in ckeditor wysiwyg mode.
  $options['enabled_buttons_def']['cmd_ckgeshi'] = 0;

  // Load the available languages from GeshiFitler module, and store them
  // in an array starting @ index:0.
  $options['available_languages'] = array();
  $languages = _geshifilter_get_enabled_languages();
  $tags = array_keys($languages);
  foreach ($tags as $tag) {
    $options['available_languages'][] = array($languages[$tag], $tag);
  }

  // Convert numeric space-counter for tab key, into text to be
  // inserted/removed by javascript when tab key is pressed.
  $options['tabKeyText'] = '';
  for ($i = 0; $i < $options['tabKeySize']; $i++) {
    $options['tabKeyText'] .= ' ';
  }

  // Replace any !path references by admin-page-configured-style for inputType,
  // with the public_html relative path, for use in ckeditor wysiwyg mode.
  $options['inputStyle'] = str_replace('[!path]', $path, $options['inputStyle']);
  $options['inputStyle'] = str_replace('\n', ' ', $options['inputStyle']);
  

  // Remove options that javascript does not need, that were primarily used for
  // drupal admin-page configuration.
  unset($options['tabKeySize']);
}

/**
 * Obtain available tags based upon input format (or NULL for global) , and
 *   formulate an array indexed by TagStyle Definitions from GeshiFilter module.
 *   Note that the 'rgx_' prefix items are precalculated regular expression
 *   equivalents, so that javascript need not generate them.
 *
 *  Here's an example output for the tag 'code', when GeshiFilter format config
 *  is set to use all permitted tag styles (fyi: 'phpblock' disabled currently).
 *  [*] Note: quotations are not actually present on output, & are here merely
 *            for visual purposes of string values....
 *  [*] Note: The indexes of the array correspond to GeSHiFilter module's 
 *            bracket types as commented below....
 *
 *  array(3) [
 *    [1]:              // ie: GESHIFILTER_BRACKETS_ANGLE
 *      pre:  "<code "
 *      post:  ">"
 *      end:  "</code>"
 *      rgx_pre:  "\<code "
 *      rgx_post:  "\>"
 *      rgx_end:  "\</code\>"
 *    [2]:              // ie: GESHIFILTER_BRACKETS_SQUARE
 *      pre:  "[code "
 *      post:  "]"
 *      end:  "[/code]"
 *      rgx_pre:  "\[code "
 *      rgx_post:  "\]"
 *      rgx_end:  "\[/code\]"
 *    [4]:              // ie: GESHIFILTER_BRACKETS_DOUBLESQUARE
 *      pre:  "[[code "
 *      post:  "]]"
 *      end:  "[[/code]]"
 *      rgx_pre:  "\[\[code "
 *      rgx_post:  "\]\]"
 *      rgx_end:  "\[\[/code\]\]"
 *  ]
 */
function ckeditor_geshi_getCodeDef_byFormat($format, &$options) {
  // Use the first available tag.
  $tag = explode(' ', geshifilter_tags($format));
  $tag = $tag[0];
  $tag_styles = _geshifilter_tag_styles($format);
  // Not supported in current release, for security purposes.
  // ToDo: Needs testing on input filters like WYSIWYG, HtmlTidy, and Law.
  unset($tag_styles[GESHIFILTER_BRACKETS_PHPBLOCK]);

  // For every tagstyle that is enabled.
  foreach ($tag_styles as $type => $enabled) {
    if ($type == $enabled) {
      // Generate container array.
      $options[$type] = array();
      // And provide general values that construct the full output:
      // <<tagStyle_pre>>tagname<<tagStyle_post>>
      switch ($type) {
        case GESHIFILTER_BRACKETS_ANGLE:
          $options[$type]['pre']      = '<';
          $options[$type]['post']     = '>';
          $options[$type]['rgx_pre']  = '\\<';
          $options[$type]['rgx_post'] = '\\>';
          break;
        case GESHIFILTER_BRACKETS_SQUARE:
          $options[$type]['pre']      = '[';
          $options[$type]['post']     = ']';
          $options[$type]['rgx_pre']  = '\\[';
          $options[$type]['rgx_post'] = '\\]';
          break;
        case GESHIFILTER_BRACKETS_DOUBLESQUARE:
          $options[$type]['pre']      = '[[';
          $options[$type]['post']     = ']]';
          $options[$type]['rgx_pre']  = '\\[\\[';
          $options[$type]['rgx_post'] = '\\]\\]';
          break;
      }
      // Formulate the end tag given it's style_pre and style_post
      $options[$type]['end'] = ($options[$type]['pre'] . '/' . $tag . $options[$type]['post']);
      // And of course the regex escaped version of it.
      $options[$type]['rgx_end'] = ($options[$type]['rgx_pre'] . '/' . $tag . $options[$type]['rgx_post']);
      // Add space so that indexOf & regex in ckgeshi plugin can properly
      // locate attributes.  
      // ie::      <codelanguage="php">     is Invalid
      // whereas:: <code language="php">    is Valid. Notice the space.
      $options[$type]['pre'] .= $tag . ' ';
      $options[$type]['rgx_pre'] .= $tag . ' ';
    }
  }
}
